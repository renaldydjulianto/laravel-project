<?php

use Illuminate\Support\Facades\Route;

//Admin Namespace
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\ChangePasswordController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\WorkOrderAcController;
use App\Http\Controllers\Admin\WorkOrderPompaController;

//Controllers Namespace
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AgendaController;
use App\Http\Controllers\ArtikelController;
use App\Http\Controllers\PengumumanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//login
Route::get('/', 'AuthController@showFormLogin')->name('login');
Route::get('login', 'AuthController@showFormLogin')->name('login');
Route::post('login', 'AuthController@login');

//registration
Route::get('registrasi', 'RegisController@index')->name('regis');
Route::post('regis/form', 'RegisController@regisForm')->name('regisForm');
// Route::post('login', 'RegisController@login');

//Home
Route::get('/',[HomeController::class,'index']);
Route::get('/home',[AdminController::class,'index'])->name('index');
Route::get('/about',[HomeController::class,'about'])->name('about');
Route::get('/contact',[HomeController::class,'contact'])->name('contact');

//Artikel
// Route::get('/artikel',[ArtikelController::class,'index'])->name('artikel');
// Route::get('/artikel/search',[ArtikelController::class,'search'])->name('artikel.search');

// Route::get('/artikel/{artikel:slug}',[ArtikelController::class,'show'])->name('artikel.show');

//Pengumuman
Route::get('/pengumuman',[PengumumanController::class,'index'])->name('pengumuman');
Route::get('/pengumuman/{pengumuman:slug}',[PengumumanController::class,'show'])->name('pengumuman.show');

//WorkOrderPompa
// Route::get('/pompa',[WorkOrderPompaController::class,'index'])->name('pompa');
// Route::get('/pompa/search',[WorkOrderPompaController::class,'search'])->name('pompa.search');
// Route::get('/pompa/{pompa:slug}',[WorkOrderP\ompaController::class,'show'])->name('pompa.show');

Route::get('/pdf/{id}', 'Admin\WorkOrderAcController@pdf')->name('cetakPDF');
Route::get('/pompa/{id}', 'Admin\WorkOrderPompaController@pdf')->name('cetakPDFPompa');
Route::get('/alarm/{id}', 'Admin\WorkOrderFireAlarmController@pdf')->name('cetakPDFFireAlarm');

//Admin
Route::group(['namespace' => 'Admin','prefix' => 'admin','middleware' => ['auth']],function(){
	Route::name('admin.')->group(function(){

		Route::get('/',[AdminController::class,'index'])->name('index');
		Route::get('/profile',[ProfileController::class,'index'])->name('profile.index');
		Route::get('/change-password',[ChangePasswordController::class,'index'])->name('change-password.index');

		//Resource Controller
		Route::resource('users','UsersController');
		// Route::resource('pengumuman','PengumumanController');
		// Route::resource('agenda','AgendaController');
		Route::resource('wo_ac','WorkOrderAcController');
		// Route::resource('pompa','WorkOrderPompaController');
		// Route::resource('fire_alarm','WorkOrderFireAlarmController');
		Route::resource('perusahaan','PerusahaanController');
		Route::resource('department','DepartmentController');
		Route::resource('karyawan','KaryawanController');
		// Route::resource('alat','PeralatanController');
		// Route::resource('kategori-artikel','KategoriArtikelController');
	});
});
