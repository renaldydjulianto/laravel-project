<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Karyawan;
use App\Models\Perusahaan;
use App\Models\Department;
use App\Models\User;

class KaryawanController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
       $karyawan = Karyawan::select('employees.status','employees.id as id','department.name as department_name','company.name as company_name','employees.name','employees.address','employees.born_date')
                    ->leftjoin('company','employees.company_id','=','company.id')
                    ->leftjoin('department','employees.department_id','=','department.id')
                    ->get();
       return view('admin.karyawan.index', compact('karyawan'));
   }
   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
       $nip = "PGW-".strtoupper(substr(uniqid(),0,5));;
       $karyawan   = Karyawan::all();
       $company    = Perusahaan::all();
       $department = Department::all();
       return view('admin.karyawan.create', compact('karyawan','company','department','nip'));
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
       Karyawan::create([
           'nip'           => $request->nip,
           'name'          => $request->name,
           'company_id'    => $request->company_id,
           'department_id' => $request->department_id,
           'address'       => $request->address,
           'gender'        => $request->gender,
           'born_date'     => $request->born_date,
           'no_hp'         => $request->no_hp,
           'religion'      => $request->religion
       ]);

       return redirect()->route('admin.karyawan.index')->with('success','Data berhasil ditambah');
   }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show(Karyawan $karyawan)
   {
       return view('admin.karyawan.show',compact('karyawan'));
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function edit(Karyawan $karyawan)
   {   
       $company    = Perusahaan::all();
       $department = Department::all();
       return view('admin.karyawan.edit',compact('karyawan','company','department'));
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, Karyawan $karyawan)
   {
       $karyawan->update($request->all());
          
       return redirect()->route('admin.karyawan.index')->with('success','Data berhasil diupdate');
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy(Department $department)
   {   
       $department->delete();
       return redirect()->route('admin.department.index')->with('success','Data berhasil dihapus');
   }
}
