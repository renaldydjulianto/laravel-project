<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Perusahaan;

class PerusahaanController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
       $perusahaan = Perusahaan::all();
       return view('admin.perusahaan.index', compact('perusahaan'));
   }
   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
       return view('admin.perusahaan.create');
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
       Perusahaan::create([
           'name'    => $request->name,
           'address' => $request->address
       ]);

       return redirect()->route('admin.perusahaan.index')->with('success','Data berhasil ditambah');
   }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show(Perusahaan $perusahaan)
   {
       return view('admin.perusahaan.show',compact('perusahaan'));
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function edit(Perusahaan $perusahaan)
   {   
       return view('admin.perusahaan.edit',compact('perusahaan'));
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, Perusahaan $perusahaan)
   {
       $perusahaan->update($request->all());
          
       return redirect()->route('admin.perusahaan.index')->with('success','Data berhasil diupdate');
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy(Perusahaan $perusahaan)
   {   
       $perusahaan->delete();
       return redirect()->route('admin.perusahaan.index')->with('success','Data berhasil dihapus');
   }
}
