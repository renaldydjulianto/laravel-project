<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Perusahaan;
use App\Models\Department;

class DepartmentController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
       $department = Department::select('department.id as id','department.name as department_name','company.name as company_name')
                    ->leftjoin('company','department.company_id','=','company.id')
                    ->get();
       return view('admin.department.index', compact('department'));
   }
   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
       $company = Perusahaan::all();
       return view('admin.department.create', compact('company'));
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
       Department::create([
           'name'       => $request->name,
           'company_id' => $request->company_id
       ]);

       return redirect()->route('admin.department.index')->with('success','Data berhasil ditambah');
   }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show(Department $department)
   {
       return view('admin.department.show',compact('department'));
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function edit(Department $department)
   {   
       $company = Perusahaan::all();
       return view('admin.department.edit',compact('department','company'));
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, Department $department)
   {
       $department->update($request->all());
          
       return redirect()->route('admin.department.index')->with('success','Data berhasil diupdate');
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy(Department $department)
   {   
       $department->delete();
       return redirect()->route('admin.department.index')->with('success','Data berhasil dihapus');
   }
}
