<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Str;

class RegisController extends Controller
{
    public function index()
    {
        $hash = Str::random(5);
        $user       = "PGL" . date("y-") . $hash;
        return view('regis.regis', compact('user'));
    }

    public function regisForm(Request $request)
    {
        $dbExist = User::query()
            ->where('email',$request->email)
            ->doesntExist();
        if ($dbExist) {
            $request->request->add(['password' => bcrypt($request->password)]);
            $newUser = new User();
            $newUser->name = $request->name;
            $newUser->nip = $request->nip;
            $newUser->birth_date = $request->birth_date;
            $newUser->phone_number = $request->phone_number;
            $newUser->gender = $request->gender;
            $newUser->email = $request->email;
            $newUser->password = $request->password;
            $newUser->role = 1;
            $newUser->active_status = "Y";
            $newUser->save();

            return redirect()->route('login')->with('success','Berhasil Daftar, silahkan Login');
        } else {
            return redirect()->route('regis')->with('error','Email sudah tersedia!, silahkan masukan email yang berbeda');
        }
        
    }
}
