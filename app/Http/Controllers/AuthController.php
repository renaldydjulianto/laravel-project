<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Validator;
use Session;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function showFormLogin()
    {
        if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
            //Login Success
            return redirect()->route('index');
        }
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $rules = [
            // 'email'                 => 'required|email',
            'username'                 => 'required',
            'password'              => 'required|string'
        ];
 
        $messages = [
            'email.required'        => 'Username wajib diisi',
            // 'email.email'           => 'Email tidak valid',
            'password.required'     => 'Password wajib diisi',
            'password.string'       => 'Password harus berupa string'
        ];
 
        $validator = Validator::make($request->all(), $rules, $messages);
 
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }
 
        $data = [
            'username'     => $request->input('username'),
            'password'  => $request->input('password'),
            'active_status'  => 'Y',
        ];
 
        Auth::attempt($data);
 
        if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
            //Login Success

            
            if(Auth::user()->role==1){ //admin

            }


            if(Auth::user()->role==3){ 

            }


            return redirect()->route('index');

 
        } else { // false
            //Login Fail
            // Session::flash('error', 'Email atau password salah');
            return redirect()->route('login')->with('error','Username atau password salah!');
        }
 
    }
}
