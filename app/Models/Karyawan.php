<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    use HasFactory;

    // protected $table = 'artikel';
    protected $table = 'employees';

    protected $fillable = [
    	'id','nip','no_hp','religion','company_id','department_id','name','address','gender','born_date','status','created_at','updated_at'
    ];
}
