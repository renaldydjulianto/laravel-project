<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Perusahaan extends Model
{
    use HasFactory;

    // protected $table = 'artikel';
    protected $table = 'company';

    protected $fillable = [
    	'id','name','address','created_at','updated_at'
    ];
}
