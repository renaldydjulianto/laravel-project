<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Maintenance | Registrasi</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('templates/backend/AdminLTE-3.0.1') }}/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('templates/backend/AdminLTE-3.0.1') }}/dist/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box mb-5">
  <!-- /.login-logo -->
  <div class="card card-outline card-primary" style="margin-top: 35px">
    <div class="card-header text-center">
      <a href="#" class="h4"><b>Registrasi Akun</a>
    </div>
    <img src="/logo.jpeg" class="img-fluid rounded mt-2" width="500px">
    <hr>
    <div class="card-body">
      <x-alert></x-alert>
      {{-- <p class="login-box-msg">Daftar Akun anda disini</p> --}}
      <form action="{{ route('regisForm') }}" method="post">
        @csrf
        <div class="form-group mb-3">
          <label for="">Nama Lengkap :</label>
          <input type="text" name="name" id="name" class="form-control" required>
          <input type="hidden" name="nip" id="nip" value="{{ $user }}">
        </div>
        <div class="form-group mb-3">
          <label for="">Tanggal lahir :</label>
          <input type="date" name="birth_date" id="birth_date" class="form-control" required>
        </div>
        <div class="form-group mb-3">
          <label for="">Jenis kelamin :</label>
          <select name="gender" id="gender" class="form-control" required>
            <option value="Laki-laki">Laki-laki</option>
            <option value="Perempuan">Perempuan</option>
          </select>
        </div>
        <div class="form-group mb-3">
          <label for="">No Handphone :</label>
          <input type="number" name="phone_number" id="phone_number" class="form-control" required>
        </div>
        <div class="form-group mb-3">
          <label for="">Email :</label>
          <input value="" name="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" required>
          @error('email')
          <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
        <div class="form-group mb-3">
          <label for="">Password :</label>
          <input value="" name="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" required>
          @error('password')
          <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
        <br>
        <div class="row">
          <!-- /.col -->
          <div class="col">
            <button type="submit" class="btn btn-primary btn-block">Daftar</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mt-2">
        {{-- <a href="">I forgot my password</a> --}}

        <center><a href="/"><button class="btn btn-secondary btn-block text-white">Halaman Utama</button></a></center>
      </p>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('templates/backend/AdminLTE-3.0.1') }}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('templates/backend/AdminLTE-3.0.1') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="{{ asset('templates/backend/AdminLTE-3.0.1') }}/dist/js/adminlte.min.js"></script>
</body>
</html>