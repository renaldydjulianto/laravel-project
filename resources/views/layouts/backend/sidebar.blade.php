<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="" class="brand-link">
    {{-- <img src="{{ asset('img/icons') }}/laravel.jpg" alt="laravel Logo" class="brand-image img-circle elevation-3"
         style="opacity: .8"> --}}
    <span class="brand-text font-weight-light ml-2 font-weight-bold">Laravel-Project</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="/user.png" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">{{ auth()->user()->name }}</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    @if (Auth::user()->role==1)
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{ route('admin.index') }}" class="nav-link {{ Request::is('admin') ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.profile.index') }}" class="nav-link {{ Request::is('admin/profile') ? 'active' : '' }}">
              <i class="nav-icon fas fa-id-card"></i>
              <p>
                Profil
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.change-password.index') }}" class="nav-link {{ Request::is('admin/change-password') ? 'active' : '' }}">
              <i class="nav-icon fas fa-unlock"></i>
              <p>
                Ubah Password
              </p>
            </a>
          </li>
          <li class="nav-header">MAIN</li>
          <li class="nav-item">
            <a href="{{ route('admin.karyawan.index') }}" class="nav-link {{ Request::segment(2) == 'karyawan' ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Data Karyawan
              </p>
            </a>
          </li>
        </ul>
      </nav>
    @elseif (Auth::user()->role==0)
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{ route('admin.index') }}" class="nav-link {{ Request::is('admin') ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.profile.index') }}" class="nav-link {{ Request::is('admin/profile') ? 'active' : '' }}">
              <i class="nav-icon fas fa-id-card"></i>
              <p>
                Profil
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.change-password.index') }}" class="nav-link {{ Request::is('admin/change-password') ? 'active' : '' }}">
              <i class="nav-icon fas fa-unlock"></i>
              <p>
                Ubah Password
              </p>
            </a>
          </li>
          <li class="nav-header">MASTER DATA</li>
          <li class="nav-item">
            <a href="{{ route('admin.users.index') }}" class="nav-link {{ Request::segment(2) == 'users' ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.perusahaan.index') }}" class="nav-link {{ Request::segment(2) == 'perusahaan' ? 'active' : '' }}">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Perusahaan
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.department.index') }}" class="nav-link {{ Request::segment(2) == 'department' ? 'active' : '' }}">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Department
              </p>
            </a>
          </li>
          <li class="nav-header">MAIN</li>
          <li class="nav-item">
            <a href="{{ route('admin.karyawan.index') }}" class="nav-link {{ Request::segment(2) == 'karyawan' ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Data Karyawan
              </p>
            </a>
          </li>

          {{-- <li class="nav-header">PENGATURAN</li> --}}
          
        </ul>
      </nav>
    @endif
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>