@extends('layouts.backend.app',[
	'title' => 'Tambah Department',
	'contentTitle' => 'Tambah Department'
])

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote') }}/summernote-bs4.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/dropify') }}/dist/css/dropify.min.css">
@endpush

@section('content')

<div class="">    
    <div class="card">
        <div class="card-header">
            <a href="{{ route('admin.department.index') }}" class="btn btn-success btn-sm">Kembali</a>
        </div>
        <div class="card-body">
        <form method="POST" enctype="multipart/form-data" action="{{ route('admin.department.store') }}">
            @csrf
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="name">Nama Department</label>
                        <input type="text" name="name" id="name" class="form-control" required>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="address">Nama Perusahaan</label>
                        <select id="company_id" name="company_id" class="form-control" data-validation="[NOTEMPTY]" required>
                            <option value="">-- Pilih Perusahaan --</option>
                                @foreach($company as $key =>$row)
                                <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-sm">SIMPAN</button>
            </div>
        </form>
    </div>
</div>

@stop