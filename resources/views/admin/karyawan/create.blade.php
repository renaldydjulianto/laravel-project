@extends('layouts.backend.app',[
	'title' => 'Tambah Karyawan',
	'contentTitle' => 'Tambah Karyawan'
])

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote') }}/summernote-bs4.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/dropify') }}/dist/css/dropify.min.css">
@endpush

@section('content')

<div class="">    
    <div class="card">
        <div class="card-header">
            <a href="{{ route('admin.karyawan.index') }}" class="btn btn-success btn-sm">Kembali</a>
        </div>
        <div class="card-body">
        <form method="POST" enctype="multipart/form-data" action="{{ route('admin.karyawan.store') }}">
            @csrf
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="nip">NIP</label>
                    <input class="form-control" type="" name="nip" id="nip" placeholder="" value="{{ $nip }}" readonly>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="name">Nama karyawan</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Nama Lengkap" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="company_id">Nama Perusahaan</label>
                        <select id="company_id" name="company_id" class="form-control" placeholder="Nama Perusahaan" required>
                            <option value="">-- Pilih Perusahaan --</option>
                                @foreach($company as $key =>$row)
                                <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="department_id">Nama Department</label>
                        <select id="department_id" name="department_id" class="form-control" placeholder="Nama Department"required>
                            <option value="">-- Pilih Department --</option>
                                @foreach($department as $key =>$row)
                                <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="address">Alamat</label>
                        <input type="text" name="address" id="address" class="form-control" placeholder="Alamat" required>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="gender">Jenis Kelamin</label>
                        <select id="gender" name="gender" class="form-control" required>
                            <option value="">-- Pilih Gender --</option>
                                <option value="L">Laki-Laki</option>
                                <option value="P">Perempuan</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="born_date">Tanggal Lahir</label>
                        <input type="date" name="born_date" id="born_date" class="form-control" required>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="religion">Agama</label>
                        <select id="religion" name="religion" class="form-control" required>
                            <option value="">-- Pilih Agama --</option>
                                <option value="Atheist">Atheist</option>
                                <option value="Katolik">Katolik</option>
                                <option value="Konghucu">Konghucu</option>
                                <option value="Budha">Budha</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Islam">Islam</option>
                                <option value="Hindu">Hindu</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="no_hp">No. Handphone</label>
                        <input type="number" name="no_hp" id="no_hp" class="form-control" placeholder="Nomor Handphone" required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-sm">SIMPAN</button>
            </div>
        </form>
    </div>
</div>

@stop