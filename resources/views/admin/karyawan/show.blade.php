@extends('layouts.backend.app',[
	'title' => 'Detail Karyawan',
	'contentTitle' => 'Detail Karyawan'
])

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote') }}/summernote-bs4.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/dropify') }}/dist/css/dropify.min.css">
@endpush

@section('content')

<div class="">    
    <div class="card">
        <div class="card-header">
            <a href="{{ route('admin.karyawan.index') }}" class="btn btn-success btn-sm">Kembali</a>
        </div>
        <div class="card-body">
        <form method="GET" enctype="multipart/form-data" action="{{ route('admin.karyawan.show',$karyawan->id) }}">
            @csrf
            @method('GET')
            @if (Auth::user()->role==0)
            <div class="form-group">
                <label for="nip">NIP</label>
                <input type="text" class="form-control" type="" name="nip" id="nip" value="{{$karyawan->nip}}" placeholder="" readonly>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="name">Nama Karyawan</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{$karyawan->name}}" readonly>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="address">Alamat</label>
                        <input type="text" name="address" id="address" class="form-control" value="{{$karyawan->address}}" readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="gender">Gender</label>
                        <input type="text" name="gender" id="gender" class="form-control" value="{{$karyawan->gender}}" readonly>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="born_date">Tanggal Lahir</label>
                        <input type="text" name="born_date" id="born_date" class="form-control" value="{{$karyawan->born_date}}" readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="no_hp">No. Handphone</label>
                        <input type="text" name="no_hp" id="no_hp" class="form-control" value="{{$karyawan->no_hp}}" readonly>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="religion">Agama</label>
                        <input type="text" name="religion" id="religion" class="form-control" value="{{$karyawan->religion}}" readonly>
                    </div>
                </div>
            </div>
        @endif
        </form>
    </div>
</div>

@stop

@push('js')
<script type="text/javascript" src="{{ asset('plugins/summernote') }}/summernote-bs4.min.js"></script>
<script type="text/javascript" src="{{ asset('plugins/dropify') }}/dist/js/dropify.min.js"></script>
<script type="text/javascript">
    // $(".summernote").summernote({
    //     height:500,
    //     callbacks: {
    //     // callback for pasting text only (no formatting)
    //         onPaste: function (e) {
    //           var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
    //           e.preventDefault();
    //           bufferText = bufferText.replace(/\r?\n/g, '<br>');
    //           document.execCommand('insertHtml', false, bufferText);
    //         }
    //     }
    // })

    // $(".summernote").on("summernote.enter", function(we, e) {
    //     $(this).summernote("pasteHTML", "<br><br>");
    //     e.preventDefault();
    // });

    // $('.dropify').dropify({
    //     messages: {
    //         default: 'Drag atau Drop untuk memilih gambar',
    //         replace: 'Ganti',
    //         remove:  'Hapus',
    //         error:   'error'
    //     }
    // });

    // $('.title').keyup(function(){
    //     var title = $(this).val().toLowerCase().replace(/[&\/\\#^, +()$~%.'":*?<>{}]/g,'-');
    //     $('.slug').val(title);
    // });
</script>
@endpush