@extends('layouts.backend.app',[
    'title' => 'Karyawan',
    'contentTitle' => 'Karyawan',
])
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('templates/backend/AdminLTE-3.0.1') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush
@section('content')
<x-alert></x-alert>
<div class="row">
    <div class="col">
        <div class="card">
           @if (Auth::user()->role==0)
              <div class="card-header">
                  <a href="{{ route('admin.karyawan.create') }}" class="btn btn-primary btn-sm">+ Tambah Karyawan</a>
              </div>
            @elseif (Auth::user()->role==1)
              <div class="card-header">
              </div>  
            @else
              <div class="card-header">
              </div>  
            @endif
            <div class="card-body table-responsive">
              <table id="dataTable1" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>No</th>
                <th>Nama Karyawan</th>
                <th>Perusahaan</th>
                <th>Department</th>
                <th>Alamat</th>
                <th>Tanggal Lahir</th>
                <th>Status</th>
                {{-- <th>Alamat</th> --}}
                @if (Auth::user()->role==0)
                <th>Aksi</th>
                @elseif(Auth::user()->role==0)
                @else
                @endif
              </tr>
              </thead>
              <tbody>
              @php 
                $no=1;
              @endphp
      
              @foreach($karyawan as $karyawan)
              <tr>
                <td>{{ $no }}</td>
                <td>{{ $karyawan->name }}</td>
                <td>{{ $karyawan->company_name }}</td>
                <td>{{ $karyawan->department_name }}</td>
                <td>{{ $karyawan->address }}</td>
                <td>{{ $karyawan->born_date }}</td>
                <td>
                  @if ($karyawan->status == 'Y')
                  <span class="badge badge-success">Aktif</span>
                  @else
                  <span class="badge badge-danger">Tidak Aktif</span>
                  @endif
                </td>
                @if (Auth::user()->role==0)
                <td>
                  <div class="row ml-2">
                    <a href="{{ route('admin.karyawan.show',$karyawan->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-eye fa-fw"></i></a>
                    &nbsp
                    <a href="{{ route('admin.karyawan.edit',$karyawan->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-edit fa-fw"></i></a>
                    <form method="POST" action="{{ route('admin.karyawan.destroy',$karyawan->id) }}">
                      @csrf
                      @method('DELETE')
                      <button onclick="return confirm('Yakin hapus ?')" type="submit" class="btn btn-danger btn-sm ml-2"><i class="fas fa-trash fa-fw"></i></button>
                    </form>
                  </div>
                </td>
                @elseif (Auth::user()->role==1)
                @else
                @endif
              </tr>
              @endforeach
              </tbody>
              </table>
            </div>
            
        </div>
    </div>
</div>
@stop
@push('js')
<!-- DataTables -->
<script src="{{ asset('templates/backend/AdminLTE-3.0.1') }}/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ asset('templates/backend/AdminLTE-3.0.1') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#dataTable1").DataTable();
  });
</script>
@endpush