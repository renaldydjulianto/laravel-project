@extends('layouts.backend.app',[
	'title' => 'Edit Karyawan',
	'contentTitle' => 'Edit Karyawan'
])

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote') }}/summernote-bs4.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/dropify') }}/dist/css/dropify.min.css">
@endpush

@section('content')

<div class="">    
    <div class="card">
        <div class="card-header">
            <a href="{{ route('admin.karyawan.index') }}" class="btn btn-success btn-sm">Kembali</a>
        </div>
        <div class="card-body">
        <form method="POST" enctype="multipart/form-data" action="{{ route('admin.karyawan.update',$karyawan->id) }}">
            @csrf
            @method('PATCH')
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="nama">Nama karyawan</label>
                        <input value="{{ $karyawan->name }}" type="text" name="name" id="name" class="form-control" required>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="kode">Nama Perusahaan</label>
                        <select id="company_id" name="company_id" class="form-control" data-validation="[NOTEMPTY]" required>
                            <option value="">-- Pilih Perusahaan --</option>
                                @foreach($company as $key =>$row)
                                <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="department_id">Nama Department</label>
                        <select id="department_id" name="department_id" class="form-control" data-validation="[NOTEMPTY]" required>
                            <option value="">-- Pilih Perusahaan --</option>
                                @foreach($department as $key =>$row)
                                <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="address">Alamat</label>
                        <input value="{{ $karyawan->address }}" type="text" name="address" id="address" class="form-control" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="gender">Jenis Kelamin</label>
                        <select id="gender" name="gender" class="form-control" data-validation="[NOTEMPTY]" required>
                            <option value="">-- Pilih Gender --</option>
                                <option value="L">Laki-laki</option>
                                <option value="P">Perempuan</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="born_date">Tanggal Lahir</label>
                        <input value="{{ $karyawan->born_date }}" type="text" name="born_date" id="born_date" class="form-control" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="status">Status Aktif</label>
                        <select id="status" name="status" class="form-control" data-validation="[NOTEMPTY]" required>
                            <option value="">-- Pilih Status --</option>
                                <option value="Y">Aktif</option>
                                <option value="N">Tidak Aktif</option>
                        </select>
                    </div>
                </div>
            </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">UPDATE</button>
        </div>
        </form>
    </div>
</div>

@stop

@push('js')
<script type="text/javascript" src="{{ asset('plugins/summernote') }}/summernote-bs4.min.js"></script>
<script type="text/javascript" src="{{ asset('plugins/dropify') }}/dist/js/dropify.min.js"></script>
<script type="text/javascript">
    $(".summernote").summernote({
        height:500,
        callbacks: {
        // callback for pasting text only (no formatting)
            onPaste: function (e) {
              var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
              e.preventDefault();
              bufferText = bufferText.replace(/\r?\n/g, '<br>');
              document.execCommand('insertHtml', false, bufferText);
            }
        }
    })

    $(".summernote").on("summernote.enter", function(we, e) {
        $(this).summernote("pasteHTML", "<br><br>");
        e.preventDefault();
    });

    $('.dropify').dropify({
        messages: {
            default: 'Drag atau Drop untuk memilih gambar',
            replace: 'Ganti',
            remove:  'Hapus',
            error:   'error'
        }
    });

    $('.title').keyup(function(){
        var title = $(this).val().toLowerCase().replace(/[&\/\\#^, +()$~%.'":*?<>{}]/g,'-');
        $('.slug').val(title);
    });
</script>
@endpush